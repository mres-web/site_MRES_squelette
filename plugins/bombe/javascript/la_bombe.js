//#---------------------------------------------------#
//#  Plugin  : La bombe                               #
//#  Auteur  : Guillaume Wauquier                     #
//#  Licence : GPL                                    #
//#------------------------------------------------------------------------------------------------------#
//#  Documentation : http://www.spip-contrib.net/Plugin-La-bombe                                         #
//#------------------------------------------------------------------------------------------------------#

// Applique le nuage d'aide avec les paramètres sur un input précis
function appliquer_la_bombe_aide_nuage(input, nuage){
	
	// chercher l'input de saisie
	
	// chercher les <span> du nuage
	var nuage = $(nuage+' .tag');
	var nuage_texte = new Array();	
	
	// pour chaque span son action
	nuage
		.each(function(i){
			nuage_texte[i] = 
				$(this)
					.click(function(){
						tags_change_valeur(input,this)
					})

		});
	
	
}

function tags_change_valeur(input,element)
{
	var input = $(input);
	var element=$(element);
	var mots_attribues = splittags(input.val());
	valeur=$(element).attr("id").substring(3);
	if (mots_attribues.contains(valeur))
		{
		input.val(jointags(mots_attribues.remove(valeur)));
		$(element).removeClass("selected")
		return true;
		}
	else
		{								
		input.val(jointags((mots_attribues.unshift(valeur) > 0) ? mots_attribues : mots_attribues));
		$(element).addClass("selected");
		return false;
		}
		
}




// Nouvelle méthode pour les tableaux
// Retourne la première occurence correspondant, sinon false
Array.prototype.contains = function (ele) {
	for (var i = 0; i < this.length; i++) {
		if (this[i] == ele) {
			return true;
		}
	}
	return false;
};

// Nouvelle méthode pour les tableaux
// Supprime un élément d'un talbeau
// qu'il y soit une ou plusieurs fois
Array.prototype.remove = function (ele) {
	var arr = new Array();
	var count = 0;
	for (var i = 0; i < this.length; i++) {
		if (this[i] != ele) {
			arr[count] = this[i];
			count++;
		}
	}
	return arr;
};

// Découpe une chaîne en un tableau de mots
function splittags(txt) {
	var temp = new Array();
	var r, i, debut;
	var compteur=1;

	if (txt.match(/^[ ,"]*$/))
		return new Array();

	while (r = txt.match(/(^| )"([^"]*)"(,| |$)/)) {
		debut = txt.search(r[0]);
		txt = txt.substring(0,debut)
			+ r[1]
			+ 'compteur'+compteur
			+ r[3]
			+ txt.substring(debut+r[0].length, 100000);
		temp['compteur'+compteur] = r[2];
		compteur++;
	}
	txt = txt.split(/[, ]+/);
	for (i=0; i<txt.length; i++) {
		if (txt[i].match('^compteur[0-9]+$')) {
			txt[i] = temp[txt[i]];
			
		}
	}
	return txt;
}

// Fabrique une chaîne à partir d'un tableau de mots
// Ajoute des guillemets si un mot contient des espaces
function jointags(a) {
	var tag, sp;
	for (var i = 0; i < a.length; i++) {
		tag = a[i];
		if (tag.split('"').length == 1
		&&(tag.split(' ').length > 1
			|| tag.split(',').length > 1
		)) {
			tag = '"'+tag+'"';
		}
		a[i] = tag;
	}

	return a.join(' ');  // ici mettre ' ' si on ne veut pas de virgule et ', ' dans le cas contraire
}


function poser_la_bombe()
{
$('#lemur ul ul').hide();
$('#lemur ul li div.supertag').click(function(){
											el=$(this).parents("li");
											if(el.hasClass("ouvert"))
												{
												el.removeClass("ouvert");
												el.children("ul").slideUp();
												$("span.selected",el).each(function(){tags_change_valeur("input#cherche_mot",this)});
												}
											else
												{
												el.addClass("ouvert");
												el.children("ul").slideDown();
												//$(this).parent("li").children("ul").slideUp()
												}
											}
											);
$('#lemur ul li div.selected').parents('li').children('ul').show();
$('#lemur ul li div.selected').parents('li').addClass("ouvert");
$('#lemur ul li li span.selected').parents('li').parents('ul').show();
appliquer_la_bombe_aide_nuage("input#cherche_mot", "#nuage_tags");
}




