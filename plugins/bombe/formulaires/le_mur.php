<?php
#---------------------------------------------------#
#  Plugin  : Étiquettes                             #
#  Auteur  : RastaPopoulos                          #
#  Licence : GPL                                    #
#------------------------------------------------------------------------------------------------------#
#  Documentation : http://www.spip-contrib.net/Plugin-Etiquettes                                       #
#                                                                                                      #
#  Définition de la balise #FORMULAIRE_ETIQUETTES                                                      #
#------------------------------------------------------------------------------------------------------#

// Sécurité
if (!defined("_ECRIRE_INC_VERSION")) {
    return;
}


function formulaires_le_mur_charger_dist($type_objet, $id_objet)
{

    $valeurs = compact("type_objet", "id_objet");
    $groupe = "tags";
    $cle_objet = id_table_objet($type_objet);
    $tab_tags = array();
    // Les paramètres ont tous déjà été testés
    // Maintenant on teste si la personne a le droit d'ajouter des mots-clés au groupe choisi
    // ET si elle a le droit de modifier l'objet choisi
    // On ne va pas plus loin si pas d'autorisation


    // Mais si on modifie, le champ est rempli avec les tags liés à l'objet

    $id_groupe = lire_config('bombe/id_groupe', 1);
    $tab_groupe = array($id_groupe);

    $reponse = sql_allfetsel('id_groupe', array('mots' => 'spip_groupes_mots'),
        array(array('=', 'id_parent', sql_quote($id_groupe))));
    foreach ($reponse as $gr) {
        if (!empty($gr['id_groupe'])) {
            $tab_groupe[] = intval($gr['id_groupe']);
        }
    }

    if ($type_objet and $id_objet) {

        $reponse = sql_select('mots.id_mot as id_mot',
            array('mots' => 'spip_mots', 'liaison' => 'spip_mots_liens'),
            array(
                array('IN', 'id_groupe', '(' . implode(',', $tab_groupe) . ')'),
                array('=', 'liaison.objet', sql_quote($type_objet)),
                array('=', 'liaison.id_objet', $id_objet),
                array('=', 'mots.id_mot', 'liaison.id_mot')
            ),
            "", "mots.titre");


        while ($mot = sql_fetch($reponse)) {

            array_push($tab_tags, $mot['id_mot']);


        }

    }


    $valeurs['tab_tags'] = $tab_tags;
    $valeurs['name'] = "cherche_mot";

    return $valeurs;


}

function formulaires_le_mur_verifier_dist($type_objet, $id_objet)
{

    $erreurs = array();

    return $erreurs;

}

function formulaires_le_mur_traiter_dist($type_objet, $id_objet)
{

    //$identifiant = etiquettes_produire_id($groupe, $type_objet, $id_objet);
    $groupe = lire_config('bombe/id_groupe');
    $cle_objet = id_table_objet($type_objet);
    $remplacer = true;
    // si on vient du formulaire validé on le traite

    // On récupère les tags
    $tags = trim(_request('cherche_mot'));

    include_spip('inc/bombe-machine');
    retirer_tous_les_mots($groupe, $type_objet, $id_objet);

    ajouter_liste_mots(
        $tags,
        $id_objet,
        $groupe,
        $type_objet,
        $cle_objet,
        $remplacer
    );

    // Si on a modifié, on renvoie la liste telle quelle, ça évite une requête pour rien
    if ($remplacer) {
        $tags = entites_html($tags);
    } // Sinon c'est un formulaire d'ajout donc il apparaît toujours vide
    else {
        $tags = "";
    }

    // On dit qu'il faut recalculer tout vu qu'on a changé
    include_spip("inc/invalideur");
    suivre_invalideur("1");

    // Relance la page
    include_spip('inc/headers');
    redirige_formulaire(self());

}
