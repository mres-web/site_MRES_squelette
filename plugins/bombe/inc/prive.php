<?php

include_spip('base/abstract_sql');
include_spip('inc/vieilles_defs');
include_spip('inc/autoriser');
include_spip('inc/utils');

function bombe_boitier($id,$table,$exec) {
	global $spip_lang_left, $spip_lang_right;
	$pkey = id_table_objet($table);
	// on recupere l'id de l'auteur en cours
	if ($GLOBALS["auteur_session"])
		$id_auteur_session = $GLOBALS['auteur_session']['id_auteur'];
	// et on verifie qu'il est autorisé à modifier l'élément en cours
	$autoriser = autoriser("associermots",$table,$id);
	echo($autoriser.":ddd");
	if($autoriser){
		$nb=sql_countsel("spip_mots_".$table."s", $pkey."=".$id);
		$deplier = ($nb > 0);
		$s = debut_cadre_enfonce("images/bombe-24.png",false,'',bouton_block_depliable(_T('bombe:communes').'</span>', $deplier, "cadre_bombes"));
		$s .= debut_block_depliable($deplier,"cadre_bombes");
		$s .= '<div id="cadre_communes" class="formulaire_spip formulaire_editer formulaire_cfg" style="margin-bottom:5px;">';
		$s .=  recuperer_fond("prive", array('id_article'=>$id));
		$s .= '</div>';
		$s .= fin_block();
		$s .= fin_cadre(true);
	}
	return $s;
}

