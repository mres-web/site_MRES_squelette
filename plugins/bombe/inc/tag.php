<?php

//	  inc_tag-machine.php
//
//    Librairies pour ajouter des mots clefs sur les objets spip à partir
//    d'un simple champ texte.
//    Distribué sans garantie sous licence GPL.
//
//    Authors  BoOz, Pierre ANDREWS, RastaPopoulos (réécriture nouvelle API)
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


if (!defined("_ECRIRE_INC_VERSION")) exit;



// Objet Tag
class Tag {
	
	// Propriétés
	var $titre;
	var $type;
	var $id_mot;
	var $id_groupe;
	


	// Constructeur
	function Tag($titre,  $id_groupe='',$id_mot=0) {
		$this->titre = $titre;
		$this->id_groupe = $id_groupe;
		if($id_mot!=0)
		{
			
			$this->id_mot = $id_mot;
			$select = sql_select(
							array('titre','type','id_groupe'), 
							'spip_mots', 
							array(array('=', 'id_mot', $this->id_mot))
							);
			if($ligne = sql_fetch($select)) {
					$this->titre = $ligne['titre'];
					$this->id_groupe = $ligne['id_groupe'];
					$this->type = $ligne['type'];
				
			}
		}
	}
	
	// Accesseurs
	function getID() {return $this->id_mot;} // public
	function getIDGroupe() {return $this->id_groupe;} // public
	function getTitre() {return $this->titre;} // public
	function getType() {return $this->type;} // public
	function getTitreEchappe() { // public
		return (strpos($this->titre,' ') || strpos($this->titre,':') || strpos($this->titre,',')) ? '"'.$this->titre.'"' : $this->titre;
	}
	function getTypeEchappe() { // public
		return (strpos($this->type,' ') || strpos($this->type,':') || strpos($this->type,',')) ? '"'.$this->type.'"' : $this->type;
	}
	
	// Renvoie une chaîne 'groupe:titre'
	function echapper() { // public
		$cgroupe = $this->type;
		$ctag = $this->titre;
		
		$cgroupe = (strpos($cgroupe,' ') || strpos($cgroupe,':') || strpos($cgroupe,','))?'"'.$cgroupe.'"':$cgroupe;
		$ctag = (strpos($ctag,' ') || strpos($ctag,':') || strpos($ctag,','))?'"'.$ctag.'"':$ctag;
		
		return (($cgroupe)? ($cgroupe.':'):'').$ctag;
	}
	
	
	/* Fonctions de vérification */
	/* ------------------------- */
	
	
	// Vérifie le groupe
	// Renvoie un tableau avec les infos : (id_groupe,unseul,titre_du_groupe)
	function verifier_groupe() { // private
		
		include_spip ('base/abstract_sql');
		
		// On va garder en mémoire les groupes vérifiés
		static $groupes_verifie;
		static $groupes_verifie_id;
		
		if($this->type) {
			
			if(!isset($groupes_verifie[$this->type])) {
				$select_groupe = sql_select(
					array('id_groupe','unseul'),
					'spip_groupes_mots',
					array(array('=', 'titre', _q($this->type)))
				);
				
				if($groupe_ligne = sql_fetch($select_groupe)) {
					$id = $groupe_ligne['id_groupe']; 
					$unseul = $groupe_ligne['unseul'];
					$groupes_verifie[$this->type] = array($id,$unseul,$this->type);
				}
				if ($select_groupe) sql_free($select_groupe);
			}
			return $groupes_verifie[$this->type];
			
		}
		else if($this->id_groupe) {
			
			if(!isset($groupes_verifie_id[$this->id_groupe])) {
				
				$select_groupe = sql_select(
					array('titre','unseul'), 
					'spip_groupes_mots', 
					array(array('=', 'id_groupe', $this->id_groupe))
				);
				
				if($groupe_ligne = sql_fetch($select_groupe)) {
					$type = $groupe_ligne['titre']; 
					$unseul = $groupe_ligne['unseul'];
					$groupes_verifie_id[$this->id_groupe] = array($this->id_groupe,$unseul,$type);
				}
				if ($select_groupe) sql_free($select_groupe);
			}
			return $groupes_verifie_id[$this->id_groupe];
			
		}
		
		// si pas de groupe
		return array(0,'');
		
	}
	
	// Vérifie si on peut bien ajouter un mot de ce groupe
	// Crée le groupe du mot s'il n'existe pas
	// Retourne l'id_groupe si c'est ok, false sinon
	function verifier($nom_objet) { // private
	
		include_spip('base/abstract_sql');
		
		list($id_groupe,$unseul,$titre) = $this->verifier_groupe();
		
		if($id_groupe > 0) {
			
			// si le groupe n'accepte qu'un mot actif
			if ($unseul == 'oui') {
				// on verifie qu'il y a pas déjà un mot associé
				$celcount = sql_select(
					'count(id_mot) as tot', 
					array(
						'spip_mots as mots',
						"spip_mots_$nom_objet as objets"
					), 
					array(
						"mots.id_groupe = $id_groupe",
						"mots.id_mot = objets.id_mot"
					),
					'mots.id_groupe'
				);
				// mot déjà utilisé, on arrête
				if($numrow = sql_fetch($celcount) && $numrow['tot'] > 0)
					return false;
				if ($celcount) sql_free($celcount);
			}
			
		}

		return $id_groupe;
	}
	
	
	/* Fonctions de modification */
	/* ------------------------- */
	
	
	// Teste si le mot existe sinon crée le mot
	// Renvoie l'id_mot
	function creer($nom_objet) { // private
		
		include_spip ('base/abstract_sql');
		
		if(!$this->id_mot && ($this->id_groupe = $this->verifier($nom_objet)) > 0) {
			
			$where = array(array('=', 'titre', _q($this->titre)));
			
			if($this->id_groupe)
				$where[] = array('=', 'id_groupe', $this->id_groupe);
			
			$result = sql_select('id_mot', 'spip_mots', $where);
			
			if ($row = sql_fetch($result))
				$this->id_mot = $row['id_mot'];
			else if($this->id_groupe) {
				spip_log("Creer le mot $this->type:$this->titre ($this->id_mot)");
				
				$this->id_mot = sql_insertq(
					'spip_mots',
					array(
						'id_groupe' => $this->id_groupe,
						'type' => $this->type,
						'titre' => $this->titre
					)
				);
			}
			if ($result) sql_free($result);	
			 
		}
		return $this->id_mot;
		
	}
	
	// Ajoute le mot à un objet quelconque
	// Sauf s'il est déjà associé
	function ajouter($id, $nom_objet, $id_objet) { // public
		
		include_spip ('base/abstract_sql');
		
		if($id) {
			// on vérifie que le mot est bien créé
			if(!$this->id_mot) {
				$this->creer($nom_objet);
			}
			
			$where = array(
				array('=', 'id_mot', $this->id_mot),
				array('=', 'objet', sql_quote($nom_objet)),
				array('=', 'id_objet', $id)
			);
			
			$result = sql_select('id_mot', "spip_mots_liens", $where);
			
			// on crée une liaison seulement si c'est pas déjà le cas
			if (sql_count($result) == 0) {
				sql_insertq(
					"spip_mots_liens",
					array(
						'id_mot' => $this->id_mot,
						'objet'=> $nom_objet,
						'id_objet'=> $id)
				);
			}
			if ($result) sql_free($result);	 
		}
		else spip_log("id_objet non défini");
		
	}
	
	// Retire le mot d'un objet quelconque
	function retirer($id, $nom_objet, $id_objet) { // public
		
		include_spip ('base/abstract_sql');
		

			sql_delete(
				"spip_mots_liens",
				"id_mot = ".intval($id)." and objet=".sql_quote($nom_objet)." and id_objet=".intval($id_objet)
			);
		
		
	}
	
}




?>
