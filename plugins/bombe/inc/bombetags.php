<?php

//	  inc_tag-machine.php
//
//    Librairies pour ajouter des mots clefs sur les objets spip à partir
//    d'un simple champ texte.
//    Distribué sans garantie sous licence GPL.
//
//    Authors  BoOz, Pierre ANDREWS, RastaPopoulos (réécriture nouvelle API)
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


if (!defined("_ECRIRE_INC_VERSION")) exit;



// Objet composé d'une liste d'objet Tag
class BombeTags {
	
	// Propriétés
	var $tags = array(); // private, la liste des mots
	var $tab_groupe;
	var $id_groupe;
	var $id_objet; // clé primaire de l'objet auquel on veut lier la liste
	
	// Constructeur
	function BombeTags($liste_tags,	$id_groupe='') { // public
		

		$this->id_groupe = $id_groupe;
		
		include_spip ('base/abstract_sql');
		$tab_groupe=array($id_groupe);
		$reponse = sql_allfetsel('id_groupe',array('mots' => 'spip_groupes_mots'),array(array('=', 'id_parent', sql_quote($id_groupe))));
		foreach($reponse as $gr)
			{
			$tab_groupe[]=$gr['id_groupe'];
			}
		$this->tab_groupe=$tab_groupe;
		// si la liste est une chaîne, il faut extraire
		if(!is_array($liste_tags))
			$this->tags = $this->parser_liste($liste_tags);
		else
			$this->tags = $liste_tags;
		
	}
	
	// Accesseurs
	function getTags() {return $this->tags;} // public
	
	// Retourne les id_mot des mots reconnus sans les créer
	// (ne retourne que ce qui existe déjà dans la base)
	function getTagsIDs() {
		
		//?? Aller chercher les tags dans la boite
		//?? pour faire plus generique : se baser sur id_$objet et/ou url_propre
		//?? car " dans l'url arrive ici sous la forme &quot; (#ENV{tags} et non #ENV*{tags})
		
		include_spip ('base/abstract_sql');
		$ids_mot = array();
		
		foreach ($this->tags as $mot) {
			if (strlen($mot->titre)) {
				$where = array(array('=', 'titre', _q($mot->titre)));
				
				if(strlen($mot->type))
					$where[] = array('=', 'type', _q($mot->type));
				
				$results = sql_select('id_mot',	'spip_mots', $where); //+ url_propre ? id_objet ?
				
				list($id) = sql_fetch($results,SPIP_NUM);
				if ($id) $ids_mot[] = $id;
				if ($results) sql_free($results);
			}
		}
		return $ids_mot;
		
	}
	
	// Retourne un tableau de chaînes groupe:titre pour chaque mot
	function toStringArray() {
		
		$retour = array();
		foreach($this->tags as $tag) {
			$retour[] = $tag->echapper();
		}
		return $retour;
		
	}
	
	
	/* Fonctions de modification */
	/* ------------------------- */
	
	
	// Ajouter tous les mots de la liste à un objet quelconque, et supprimer les anciens si ya l'option clear
	function ajouter($id, $nom_objet='documents', $id_objet='id_document', $clear=false) { // public
		
		include_spip ('base/abstract_sql');

		if($id) {
			
			// si il y a l'option clear, on efface les anciennes liaisons avant
			if ($clear) {
			

				$result = sql_select(
					'id_mot',
					'spip_mots',
					'spip_mots.id_groupe IN ('.implode(',',$this->tab_groupe).')'
				);

				$mots_a_effacer = array('0');
				
				while ($row = sql_fetch($result)) {
					$mots_a_effacer[] = $row['id_mot']; 
				}
				if ($result) sql_free($result);
				
				spip_log("Enleve les mots: (".join(',',$mots_a_effacer).") à (".$id_objet.", ".intval($id).")");
				sql_delete(
					"spip_mots_$nom_objet",
					$id_objet." = ".intval($id)." and id_mot in (".join(',', $mots_a_effacer).")"
				);
			}
			
			// ensuite on ajoute chaque mot
			foreach($this->tags as $mot) {
				if (trim($mot->titre) != "")
				$mot->ajouter($id,$nom_objet,$id_objet);
			}
			
		}
		
	}
	
	// Retirer les mots de la liste d'un objet quelconque
	function retirer($id, $nom_objet='documents', $id_objet='id_document') {
		
		include_spip ('base/abstract_sql');
		
		if($id) {
			foreach($this->tags as $mot) {
				$mot->retirer($id,$nom_objet,$id_objet);
			}
		}
		
	}
	
	
	/* Fonctions statiques */
	/* ------------------- */
	
	// Prend un chaîne et fabrique un tableau d'objets Tag
	function parser_liste($liste_tags) { // private static
		
		$liste_tags = trim($liste_tags);
		

		// exploser selon les tab
		$tags = explode(" ",$liste_tags);
		

		// recuperer les groupes sous la forme  <groupe:mot>
		foreach ($tags as $i => $id_tag) {
			$tags[$i] = new Tag("","",$id_tag);
		}
		
		return $tags;
		
	}
	
	function containsSeparateur($char) {
		return (strpos($char,' ') || strpos($char,':') || strpos($char,','));
	}
	
	
}

?>
