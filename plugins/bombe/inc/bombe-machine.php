<?php

//	  inc_tag-machine.php
//
//    Librairies pour ajouter des mots clefs sur les objets spip à partir
//    d'un simple champ texte.
//    Distribué sans garantie sous licence GPL.
//
//    Authors  BoOz, Pierre ANDREWS, RastaPopoulos (réécriture nouvelle API)
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


if (!defined("_ECRIRE_INC_VERSION")) {
    exit;
}


include_spip('inc/bombetags');
include_spip('inc/tag');


function entableau($tag)
{
    return array('groupe' => $tag->type, 'tag' => $tag->titre);
}


/*
	Ajoute les mots clefs dans la liste passée en paramètre au bon objet.
	Si le mot clef n'existe pas, on le crée
	Si le groupe n'existe pas, on le crée
	
	Paramètres :
	$tags: tableau de tag ('groupe' => groupe, 'tag' => tag)
	$id: id de l'objet sur lequel ajouter les mots clefs
	[$groupe_defaut]: groupe par défaut pour les mots qui n'ont pas de groupe dans la chaîne
	[$nom_objet]: type d'objet sur lequel ajouter les mots clefs (une table: spip_mots_$nom_objet doit exister)
	[$id_objet]: colonne de la table de cet objet qui contient les ids
	
	Retourne:
	rien
*/


function retirer_tous_les_mots($id_groupe, $type_objet, $id_objet)
{


    $reponse = sql_allfetsel('id_groupe',array('mots' => 'spip_groupes_mots'),array(array('=', 'id_parent', sql_quote($id_groupe))));
    foreach($reponse as $gr)
    {
        $tab_groupe[]=$gr['id_groupe'];
    }


    $tab_mots = sql_allfetsel(
        'id_mot',
        'spip_mots',
        'spip_mots.id_groupe IN (' . implode(',', $tab_groupe) . ')'
    );
    foreach ($tab_mots as &$m) {
        $m = $m['id_mot'];
    }
    sql_delete('spip_mots_liens',
        'objet=' . sql_quote($type_objet) . ' and id_objet=' . $id_objet . ' and id_mot IN (' . implode(',', $tab_mots) . ')');

}


function ajouter_liste_mots(
    $tags,
    $id,
    $id_groupe_defaut = '',
    $nom_objet = 'documents',
    $id_objet = 'id_document',
    $clear = true
) {

    $tags = new BombeTags($tags, $id_groupe_defaut);
    $tags->ajouter($id, $nom_objet, $id_objet, $clear);
}

function ajouter_mots(
    $liste_tags,
    $id,
    $groupe_defaut = '',
    $nom_objet = 'documents',
    $id_objet = 'id_document',
    $clear = true
) {
    $tags = new BombeTags($liste_tags, $groupe_defaut);
    $tags->ajouter($id, $nom_objet, $id_objet, $clear);
}


/**
 * Enleve les mots clefs passé en paramètre
 *
 * Paramètres :
 * $tags: tableau de tag ('groupe' => groupe, 'tag' => tag)
 * $id: id de l'objet sur lequel supprimer les mots clefs
 * [groupe_defaut]: groupe par défaut pour les mots qui n'ont pas de groupe dans la chaîne
 * [$nom_objet]: type d'objet sur lequel supprimer les mots clefs (une table: spip_mots_$nom_objet doit exister)
 * [$id_objet]: colonne de la table de cet objet qui contient les ids
 *
 * Retourne:
 * rien
 */
function retirer_liste_mots(
    $tags,
    $id,
    $groupe_defaut = '',
    $nom_objet = 'documents',
    $id_objet = 'id_document'
) {
    $tags = new BombeTags($tags, $groupe_defaut);
    $tags->retirer($id, $nom_objet, $id_objet);
}

function retirer_mots(
    $liste_tags,
    $id,
    $groupe_defaut = '',
    $nom_objet = 'documents',
    $id_objet = 'id_document'
) {
    $tags = new BombeTags($liste_tags, $groupe_defaut);
    $tags->retirer($id, $nom_objet, $id_objet);
}


function parser_liste($liste_tags)
{
    $tags = new BombeTags($liste_tags, '');
    return array_map('entableau', $tags->getTags());
}

