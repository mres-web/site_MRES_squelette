<?php
/**
 * Fonctions du squelette associé
 *
 * @plugin     Annuaire des associations
 * @copyright  2017
 * @author     Guillaume Wauquier
 * @licence    GNU/GPL
 * @package    SPIP\Mres_asso\Fonctions
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


// pour initiale et afficher_initiale
include_spip('prive/objets/liste/auteurs_fonctions');
