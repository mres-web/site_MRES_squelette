<?php
/**
 * Options au chargement du plugin Annuaire des associations
 *
 * @plugin     Annuaire des associations
 * @copyright  2017
 * @author     Guillaume Wauquier
 * @licence    GNU/GPL
 * @package    SPIP\Mres_asso\Options
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
