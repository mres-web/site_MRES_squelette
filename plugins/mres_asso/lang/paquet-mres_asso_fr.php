<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// M
	'mres_asso_description' => 'Liste des associations de la MRES',
	'mres_asso_nom' => 'Annuaire des associations',
	'mres_asso_slogan' => 'Tout sur les associations du réséau',
);
