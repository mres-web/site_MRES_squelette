<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/albums/trunk/lang/
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(


	'info_titre_emploi' =>'Intitulé du poste (pas plus de 200 caractères.)',
	'info_nom_stucture' =>'Nom de votre structure',
	'info_structure' =>'Description de votre structure',
	'info_poste' =>'Description du poste',
	'info_competence' =>'Compétences',
	'info_formalite' =>'Formalités pour adresser sa réponse',

	'placeholder_structure' =>'Description de votre structure, son objet, un peu d\'historique',
	'placeholder_poste' =>'Description du poste, résumé de la fiche de poste',
	'placeholder_competence' =>'Description des compétences requises pour cet emploi',
	'placeholder_formalite' =>'Comment et qui joindre pour répondre à cette offre ',



);
