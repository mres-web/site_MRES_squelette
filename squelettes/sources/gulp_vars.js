

const config = {
    nodeDir: './node_modules/',
    bootstrapDir: './node_modules/bootstrap-sass/assets/',
    dir: './',
    publicDir: './../',
    debug:false
};


const fichiers = {

    'bootstrap': [
        config.dir + 'scss/bootstrap.scss',
    ],

    'sass': [
        config.dir + 'scss/animation.scss',
        config.dir + 'scss/la_bombe.css',
        config.dir + 'scss/perso.scss'
    ],
    'styles': [
        config.nodeDir + 'font-awesome/css/font-awesome.css',
        config.nodeDir + 'select2/dist/css/select2.css',
        config.nodeDir + 'daterangepicker/daterangepicker.css',
        config.nodeDir + 'sidr/dist/stylesheets/jquery.sidr.light.css',
        //config.dir + 'css/fonts.css',
        config.dir + 'css/bootstrap.css',
        config.dir + 'css/styles_sass.css'
    ],
    'scripts': [
        config.nodeDir + 'sidr/dist/jquery.sidr.js',
        config.nodeDir + 'isotope-layout/dist/isotope.pkgd.js',
        config.nodeDir +'select2/dist/js/select2.js',
        config.nodeDir +'select2/dist/js/select2.full.js',
        config.nodeDir +'select2/dist/js/i18n/fr.js',
        config.nodeDir + 'daterangepicker/moment.min.js',
        config.nodeDir + 'daterangepicker/daterangepicker.js',

        config.nodeDir + 'bootstrap-datepicker/js/bootstrap-datepicker.js',
        config.nodeDir + 'bootstrap-datepicker/js/locales/bootstrap-datepicker.fr.js',
        config.nodeDir + 'bootstrap-timepicker/js/bootstrap-timepicker.js',
        config.nodeDir +'velocity-animate/velocity.js',
        config.nodeDir +'velocity-animate/velocity.ui.js',
        config.dir + '/js/*.js'
    ],
    'scripts_bas_de_page': [
        config.bootstrapDir + '/javascripts/bootstrap.js',
        config.dir+'/js/lunr/mustache.js',
        config.dir+'/js/lunr/elasticlunr.min.js',
        config.dir+'/js/lunr/lunr.stremmer.support.min.js',
        config.dir+'/js/lunr/lunr.fr.min.js',
        config.dir+'/js/lunr/lunr.js'
    ],

};


const icones_fa = {
    fal: [],
    far: [],
    fas: ['rocket','times','plus','circle','cogs','globe','envelope','angle-double-down','download','map-marker','search','eye','tags','tag','chevron-right','bug','redo','exclamation-triangle','calendar','newspaper','bars','print','sign-out-alt','user','file-alt','file-audio','file-image','file-video'],
    fab: ['mastodon','twitter','facebook','linux','linkedin']
};


module.exports = [config,fichiers,icones_fa];