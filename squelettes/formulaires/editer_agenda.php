<?php

/***************************************************************************\
 *  SPIP, Systeme de publication pour l'internet                           *
 *                                                                         *
 *  Copyright (c) 2001-2017                                                *
 *  Arnaud Martin, Antoine Pitrou, Philippe Riviere, Emmanuel Saint-James  *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\***************************************************************************/

/**
 * Gestion du formulaire de d'édition d'evenement
 *
 * @package SPIP\Core\evenements\Formulaires
 **/


include_spip('inc/actions');
include_spip('inc/editer');

/**
 * Chargement du formulaire d'édition d'evenement
 *
 * @uses formulaires_editer_objet_charger()
 *
 * @param int|string $id_evenement
 *     Identifiant de l'evenement. 'new' pour une nouvel evenement.
 * @param int $id_article
 *     Identifiant de la article parente
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un evenement source de traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL de l'evenement, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return array
 *     Environnement du formulaire
 **/
function formulaires_editer_agenda_charger_dist(
	$id_evenement = 'new',
	$id_article = 0,
	$retour = '',
	$lier_trad = 0,
	$config_fonc = 'evenements_edit_config',
	$row = array(),
	$hidden = ''
) {
	$valeurs = formulaires_editer_objet_charger(
		'evenement',
		$id_evenement,
		$id_article,
		$lier_trad,
		$retour,
		$config_fonc,
		$row,
		$hidden
	);


	$valeurs['id_parent']= 19;

	$tab_asso=[];
	$tab_asso_sql = sql_allfetsel("id_association,nom,sigle",'spip_associations');
	foreach($tab_asso_sql as $mot)
		if (!empty($mot['sigle']) && $mot['sigle']!= $mot['nom'])
			$tab_asso[$mot['id_association']]=$mot['sigle'].' - '.substr($mot['nom'],0,25).((strlen($mot['nom'])>25)?'...':'');
		else
			$tab_asso[$mot['id_association']]=$mot['nom'];
	$valeurs['tab_associations']= $tab_asso;


	$tab_tags = array();
	$id_groupe = lire_config('bombe/id_groupe');
	$reponse = sql_allfetsel('id_groupe,id_mot', array('mots' => 'spip_mots'),'id_groupe='.intval($id_groupe));
	//foreach ($reponse as $gr) {  $tab_tags[] = intval($gr['id_mot']); }
		$valeurs['tab_tags'] = $tab_tags;


	$tab_mot=array();
	$tab_mot_sql = sql_allfetsel("id_mot,titre","spip_mots","id_groupe=15");
	foreach($tab_mot_sql as &$mot)
		$tab_mot[$mot['id_mot']]=$mot['titre'];
	$valeurs['mots_datas']= $tab_mot;


	$valeurs['tab_zone']=array('mres'=>'MRES','mel'=>'MEL','npdc'=>'Nord Pas-de-Calais','autre'=>'Ailleurs');

	return $valeurs;
}


function formulaires_editer_agenda_fichiers(){
	return array('image');
}




/**
 * Identifier le formulaire en faisant abstraction des paramètres qui
 * ne représentent pas l'objet édité
 *
 * @param int|string $id_evenement
 *     Identifiant de l'evenement. 'new' pour une nouvel evenement.
 * @param int $id_article
 *     Identifiant de la article parente
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un evenement source de traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL de l'evenement, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return string
 *     Hash du formulaire
 */
function formulaires_editer_agenda_identifier_dist(
	$id_evenement = 'new',
	$id_article = 0,
	$retour = '',
	$lier_trad = 0,
	$config_fonc = 'evenements_edit_config',
	$row = array(),
	$hidden = ''
) {
	return serialize(array(intval($id_evenement), $lier_trad));
}

/**
 * Choix par défaut des options de présentation
 *
 * @param array $row
 *     Valeurs de la ligne SQL d'un evenement, si connu
 * return array
 *     Configuration pour le formulaire
 */
function evenements_edit_config($row) {
	global $spip_lang;

	$config = $GLOBALS['meta'];
	$config['lignes'] = 8;
	$config['langue'] = $spip_lang;
	$config['restreint'] = ($row['statut'] == 'publie');

	return $config;
}

/**
 * Vérifications du formulaire d'édition d'evenement
 *
 * @uses formulaires_editer_objet_verifier()
 *
 * @param int|string $id_evenement
 *     Identifiant de l'evenement. 'new' pour une nouvel evenement.
 * @param int $id_article
 *     Identifiant de la article parente
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un evenement source de traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL de l'evenement, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return array
 *     Erreurs du formulaire
 **/
function formulaires_editer_agenda_verifier_dist(
	$id_evenement = 'new',
	$id_article = 0,
	$retour = '',
	$lier_trad = 0,
	$config_fonc = 'evenements_edit_config',
	$row = array(),
	$hidden = ''
) {
	// auto-renseigner le titre si il n'existe pas
	titre_automatique('titre', array('descriptif', 'chapo', 'texte'));
	// on ne demande pas le titre obligatoire : il sera rempli a la volee dans editer_texte si vide
	$erreurs = formulaires_editer_objet_verifier('evenement', $id_evenement, array('id_parent'));
	// si on utilise le formulaire dans le public
	if (!function_exists('autoriser')) {
		include_spip('inc/autoriser');
	}
	if (!isset($erreurs['id_parent'])
		and !autoriser('creerevenementdans', 'article', _request('id_parent'))
	) {
		$erreurs['id_parent'] = _T('info_creerdansarticle_non_autorise');
	}

	return $erreurs;
}

/**
 * Traitements du formulaire d'édition d'evenement
 *
 * @uses formulaires_editer_objet_traiter()
 *
 * @param int|string $id_evenement
 *     Identifiant de l'evenement. 'new' pour une nouvel evenement.
 * @param int $id_article
 *     Identifiant de la article parente
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un evenement source de traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL de l'evenement, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return array
 *     Retours des traitements
 **/
function formulaires_editer_agenda_traiter_dist(
	$id_evenement = 'new',
	$id_article = 0,
	$retour = '',
	$lier_trad = 0,
	$config_fonc = 'evenements_edit_config',
	$row = array(),
	$hidden = ''
) {
	$datetimes = _request('datetimes');
	$datetimes = explode(' - ',$datetimes);
	$date_debut=date_create_from_format('d/m/Y H:i',$datetimes[0]);
	$date_fin=date_create_from_format('d/m/Y H:i',$datetimes[1]);
	set_request('date_debut',$date_debut->format('Y-m-d H:i:s'));
	set_request('date_fin',$date_fin->format('Y-m-d H:i:s'));
	$res  = formulaires_editer_objet_traiter(
		'evenement',
		$id_evenement,
		$id_article,
		$lier_trad,
		$retour,
		$config_fonc,
		$row,
		$hidden
	);


	if (isset($res['message_ok'])){

		if ($id_objet = intval($res['id_evenement'])
		and autoriser('instituer', 'evenement', $id_objet, '', array('statut' => 'publie'))
		) {
			include_spip('action/editer_objet');
			objet_modifier('evenement', $id_objet, array('statut' => 'publie'));


			$tab_asso = _request('association');
			$id_objet = intval($res['id_evenement']);
			include_spip('action/editer_auteur');
			if (!empty($tab_asso)){
				foreach($tab_asso as $id_asso ){
					objet_associer(array('association' => $id_asso), array('evenement' => $id_objet));
				}
			}


			$fichiers = _request('_fichiers');

			if (isset($fichiers['image'][0])){
				$f=$fichiers['image'][0];
				if ($f['extension']=='jpeg')
					$f['extension']='jpg';
				$f_doc = 'evenementon'.$id_objet.'.'.$f['extension'];
				copy($f['tmp_name'],_DIR_IMG.$f_doc);
			}


			$tab_mot = explode(',',_request('themes'));
			include_spip('action/editer_mot');
			if (!empty($tab_mot)) {
				foreach ($tab_mot as $id_mot) {
					mot_associer($id_mot, array('evenement' => $id_objet));
				}
			}



			$tab_mot = _request('mots');
			include_spip('action/editer_mot');
			if (!empty($tab_mot)) {
				foreach ($tab_mot as $mot) {
					if (intval($mot) == 0) {
						$id_mot = mot_inserer(1);
						mot_modifier($id_mot, ['titre' => $mot]);
					} else {
						$id_mot = $mot;
					}
					mot_associer($id_mot, array('evenement' => $id_objet));
				}
			}
		}
	}
	return $res;
}
