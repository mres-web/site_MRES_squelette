<?php
/**
 * Plugin mailsubscribers
 * (c) 2012 Cédric Morin
 * Licence GNU/GPL v3
 */

if (!defined('_ECRIRE_INC_VERSION')) return;

/**
 * Declarer les champs postes et y integrer les valeurs par defaut
 */
function formulaires_newsletter_inscription_charger_dist($listes=''){
	$valeurs = array(
		'session_email' => ''
	);
	if (isset($GLOBALS['visiteur_session']['email']))
		$valeurs['session_email'] = $GLOBALS['visiteur_session']['email'];
	elseif (isset($GLOBALS['visiteur_session']['session_email']))
		$valeurs['session_email'] = $GLOBALS['visiteur_session']['session_email'];

	return $valeurs;
}

/**
 * Verifier les champs postes et signaler d'eventuelles erreurs
 */
function formulaires_newsletter_inscription_verifier_dist($listes=''){

	$erreurs = array();
	if (!$email = _request('session_email')){
		$erreurs['session_email'] = _T('info_obligatoire');
	}
	else {
		// verifier que l'email est valide
		if (!email_valide($email))
			$erreurs['session_email'] = _T('info_email_invalide');
	}
	return $erreurs;
}

/**
 * Traiter les champs postes
 */
function formulaires_newsletter_inscription_traiter_dist($listes=''){

	// langue par defaut lors de l'inscription : la langue courante dans la page
	$options = array('lang'=>$GLOBALS['spip_lang']);
	$email = _request('session_email');

	$res = array(
		'editable'=>true
	);

	//$newsletter_inscription = charger_fonction("subscribe","newsletter");

	charger_fonction('notifications', 'inc');
	$message = "Une demande d'inscription vient d'être effectué à partir du site : ".$email;
	notifications_envoyer_mails("sympa@listes.mres-asso.fr?subject=SUBSCRIBE%20infomres", $message, "subscribe address=".$email."");
	/*if (){
		if (lire_config('mailsubscribers/double_optin',0))
			$res['message_ok'] = _T('newsletter:subscribe_message_ok_confirm',array('email'=>"<b>$email</b>"));
		else*/
			$res['message_ok'] = _T("Votre demande d'inscription a bien été enregistrée. Un message vient de vous être envoyé. Merci de suivre les instructions pour finaliser votre abonnement.",array('email'=>"<b>$email</b>"));
	/*}

	else
		$res['message_erreur'] = _T('mailsubscriber:erreur_technique_subscribe');
	*/
	set_request('email');
	return $res;
}
