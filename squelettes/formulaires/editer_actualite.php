<?php

/***************************************************************************\
 *  SPIP, Systeme de publication pour l'internet                           *
 *                                                                         *
 *  Copyright (c) 2001-2017                                                *
 *  Arnaud Martin, Antoine Pitrou, Philippe Riviere, Emmanuel Saint-James  *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\***************************************************************************/

/**
 * Gestion du formulaire de d'édition d'article
 *
 * @package SPIP\Core\Articles\Formulaires
 **/


include_spip('inc/actions');
include_spip('inc/editer');

/**
 * Chargement du formulaire d'édition d'article
 *
 * @uses formulaires_editer_objet_charger()
 *
 * @param int|string $id_article
 *     Identifiant de l'article. 'new' pour une nouvel article.
 * @param int $id_rubrique
 *     Identifiant de la rubrique parente
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un article source de traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL de l'article, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return array
 *     Environnement du formulaire
 **/
function formulaires_editer_actualite_charger_dist(
	$id_article = 'new',
	$id_rubrique = 0,
	$retour = '',
	$lier_trad = 0,
	$config_fonc = 'articles_edit_config',
	$row = array(),
	$hidden = ''
) {
	$valeurs = formulaires_editer_objet_charger(
		'article',
		$id_article,
		$id_rubrique,
		$lier_trad,
		$retour,
		$config_fonc,
		$row,
		$hidden
	);


	$valeurs['id_parent']= 24;

	$tab_asso=array();
	$tab_asso_sql = sql_allfetsel("id_association,nom,sigle",'spip_associations');
	foreach($tab_asso_sql as $mot)
		if (!empty($mot['sigle']) && $mot['sigle']!= $mot['nom'])
			$tab_asso[$mot['id_association']]=$mot['sigle'].' - '.substr($mot['nom'],0,25).((strlen($mot['nom'])>25)?'...':'');
		else
			$tab_asso[$mot['id_association']]=$mot['nom'];

	$valeurs['tab_associations']= $tab_asso;

	$tab_tags = array();

	$valeurs['tab_tags'] = $tab_tags;


	$tab_mot=array();
	$tab_mot_sql = sql_allfetsel("id_mot,titre","spip_mots","id_groupe=15");
	foreach($tab_mot_sql as &$mot)
		$tab_mot[$mot['id_mot']]=$mot['titre'];
	$valeurs['mots_datas']= $tab_mot;

	return $valeurs;
}


function formulaires_editer_actualite_fichiers(){
	return array('image');
}


/**
 * Identifier le formulaire en faisant abstraction des paramètres qui
 * ne représentent pas l'objet édité
 *
 * @param int|string $id_article
 *     Identifiant de l'article. 'new' pour une nouvel article.
 * @param int $id_rubrique
 *     Identifiant de la rubrique parente
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un article source de traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL de l'article, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return string
 *     Hash du formulaire
 */
function formulaires_editer_actualite_identifier_dist(
	$id_article = 'new',
	$id_rubrique = 0,
	$retour = '',
	$lier_trad = 0,
	$config_fonc = 'articles_edit_config',
	$row = array(),
	$hidden = ''
) {
	return serialize(array(intval($id_article), $lier_trad));
}

/**
 * Choix par défaut des options de présentation
 *
 * @param array $row
 *     Valeurs de la ligne SQL d'un article, si connu
 * return array
 *     Configuration pour le formulaire
 */
function articles_edit_config($row) {
	global $spip_lang;

	$config = $GLOBALS['meta'];
	$config['lignes'] = 8;
	$config['langue'] = $spip_lang;

	$config['restreint'] = ($row['statut'] == 'publie');

	return $config;
}

/**
 * Vérifications du formulaire d'édition d'article
 *
 * @uses formulaires_editer_objet_verifier()
 *
 * @param int|string $id_article
 *     Identifiant de l'article. 'new' pour une nouvel article.
 * @param int $id_rubrique
 *     Identifiant de la rubrique parente
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un article source de traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL de l'article, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return array
 *     Erreurs du formulaire
 **/
function formulaires_editer_actualite_verifier_dist(
	$id_article = 'new',
	$id_rubrique = 0,
	$retour = '',
	$lier_trad = 0,
	$config_fonc = 'articles_edit_config',
	$row = array(),
	$hidden = ''
) {
	// auto-renseigner le titre si il n'existe pas
	titre_automatique('titre', array('descriptif', 'chapo', 'texte'));
	// on ne demande pas le titre obligatoire : il sera rempli a la volee dans editer_texte si vide
	$erreurs = formulaires_editer_objet_verifier('article', $id_article, array('id_parent'));
	// si on utilise le formulaire dans le public
	if (!function_exists('autoriser')) {
		include_spip('inc/autoriser');
	}
	if (!isset($erreurs['id_parent'])
		and !autoriser('creerarticledans', 'rubrique', _request('id_parent'))
	) {
		$erreurs['id_parent'] = _T('info_creerdansrubrique_non_autorise');
	}

	if (_request('tromperie'))
		$erreurs['tromperie'] = 'Il ne fallait rien remplir.';

	// options pour vérifier les images
	// si les options ne sont pas renseignées, la vérification se base sur
	// _IMG_MAX_SIZE, _IMG_MAX_WIDTH, _IMG_MAX_HEIGHT
	$verifier = charger_fonction('verifier', 'inc', true);

	$options = array(
		'taille_max' => 2500, // en Ko
		'largeur_max' => 3000, // en px
		'hauteur_max' => 3000, // en px
	);

	// vérifier le champ image unique
	if ($erreur = $verifier($_FILES['image'], 'image_upload', $options)) {
		// renvoyer l'erreur dans le formulaire
		$erreurs['image'] = $erreur;
		// supprimer le fichier en erreur dans _FILES
		cvtupload_nettoyer_files_selon_erreurs('image',$erreur);
	}



	// vérifier le champ images multiples
	$erreurs_fichiers = array();
	if ($erreur = $verifier($_FILES['plusieurs_images'], 'image_upload_multiple', $options, $erreurs_fichiers)) {
		// renvoyer l'erreur dans le formulaire
		$erreurs['plusieurs_images'] = $erreur;
		// supprimer les fichiers en erreur dans _FILES
		foreach ($erreurs_fichiers as $id_file => $erreur) {
			foreach ($_FILES['plusieurs_images'] as $key => $val) {
				unset($_FILES['plusieurs_images'][$key][$id_file]);
			}
		}
	}





	return $erreurs;
}

/**
 * Traitements du formulaire d'édition d'article
 *
 * @uses formulaires_editer_objet_traiter()
 *
 * @param int|string $id_article
 *     Identifiant de l'article. 'new' pour une nouvel article.
 * @param int $id_rubrique
 *     Identifiant de la rubrique parente
 * @param string $retour
 *     URL de redirection après le traitement
 * @param int $lier_trad
 *     Identifiant éventuel d'un article source de traduction
 * @param string $config_fonc
 *     Nom de la fonction ajoutant des configurations particulières au formulaire
 * @param array $row
 *     Valeurs de la ligne SQL de l'article, si connu
 * @param string $hidden
 *     Contenu HTML ajouté en même temps que les champs cachés du formulaire.
 * @return array
 *     Retours des traitements
 **/
function formulaires_editer_actualite_traiter_dist(
	$id_article = 'new',
	$id_rubrique = 0,
	$retour = '',
	$lier_trad = 0,
	$config_fonc = 'articles_edit_config',
	$row = array(),
	$hidden = ''
) {





	$res  = formulaires_editer_objet_traiter(
		'article',
		$id_article,
		$id_rubrique,
		$lier_trad,
		$retour,
		$config_fonc,
		$row,
		$hidden
	);




	if (isset($res['message_ok'])){



			include_spip('action/editer_objet');
				include_spip('action/editer_liens');



				$tab_asso = _request('association');
				$id_objet = intval($res['id_article']);
				include_spip('action/editer_auteur');
				if (!empty($tab_asso)){
					foreach($tab_asso as $id_asso ){
						objet_associer(array('association' => $id_asso), array('article' => $id_objet));
					}
				}


			$fichiers = _request('_fichiers');

		if (isset($fichiers['image'][0])){
					$f=$fichiers['image'][0];
					if ($f['extension']=='jpeg')
						$f['extension']='jpg';
					$f_doc = 'arton'.$id_objet.'.'.$f['extension'];
					copy($f['tmp_name'],_DIR_IMG.$f_doc);
		}


		$tab_mot = explode(',',_request('themes'));
		include_spip('action/editer_mot');
		if (!empty($tab_mot)) {
			foreach ($tab_mot as $id_mot) {
				mot_associer($id_mot, array('article' => $id_objet));
			}
		}



		$tab_mot = _request('mots');
		include_spip('action/editer_mot');
		if (!empty($tab_mot)) {
			foreach ($tab_mot as $mot) {
				if (intval($mot) == 0) {
					$id_mot = mot_inserer(15);
					mot_modifier($id_mot, ['titre' => $mot]);
				} else {
					$id_mot = $mot;
				}
				mot_associer($id_mot, array('article' => $id_objet));
			}
		}

	}


		if ($id_objet = intval($res['id_article'])
			and autoriser('instituer', 'article', $id_objet, '', array('statut' => 'publie'))
		) {
			sql_updateq('spip_articles', array('statut' => 'publie'), 'id_article='.$id_objet);
		}



	return $res;
}
